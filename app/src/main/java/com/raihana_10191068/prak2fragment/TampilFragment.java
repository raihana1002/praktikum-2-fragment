package com.raihana_10191068.prak2fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class TampilFragment extends Fragment {
    Button btnklik;

    public TampilFragment(){
        //Required empty public contructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_tampil, container, false);

        btnklik = (Button) view.findViewById(R.id.btnklik);
        btnklik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "This is from fragment", Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
}